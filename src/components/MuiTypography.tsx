import { Typography } from "@mui/material";

export const MuiTypography = () => {
  return (
    <div>
      <Typography variant="h1">h1 Heading</Typography>
      <Typography variant="h2">h2 Heading</Typography>
      <Typography variant="h3">h3 Heading</Typography>

      {/* See inspect element for h4, it's look like same as h4 but heading is h1. and gutterBottom is used for bottom margin of headings */}
      <Typography variant="h4" component="h1" gutterBottom>
        h4 Heading
      </Typography>
      <Typography variant="h5">h5 Heading</Typography>
      <Typography variant="h6">h6 Heading</Typography>

      <Typography variant="subtitle1">Sub title 1</Typography>
      <Typography variant="subtitle2">Sub title 2</Typography>

      {/* <Typography variant="body1">  body1 is default */}
      <Typography>
        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Sequi corrupti
        ad saepe vero voluptate minima officia praesentium dicta odio adipisci,
        et, eligendi libero reiciendis rem iusto harum molestias itaque earum.
      </Typography>
      <Typography variant="body2">
        Lorem ipsum dolor sit amet consectetur adipisicing elit. Facere
        dignissimos at rerum distinctio officia dolorum esse culpa ullam fugiat
        deleniti aliquid libero, incidunt architecto perspiciatis quia beatae
        dolorem autem! Aperiam.
      </Typography>
    </div>
  );
};
